# Copyright 2022 Taavi Väänänen <hi@taavi.wtf>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

terraform {
  required_version = ">= 1.4.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.54.1"
    }
  }
}

variable "alertmanager_vms" {
  type = map(object({
    image  = string,
    flavor = string,
  }))
}

variable "prometheus_security_group_id" {
  type = string
}

variable "haproxy_security_group_id" {
  type = string
}

variable "thanos_fe_security_group_id" {
  type = string
}

data "openstack_compute_flavor_v2" "alertmanager_vm_flavor" {
  for_each = var.alertmanager_vms

  name = each.value.flavor
}

data "openstack_images_image_v2" "alertmanager_vm_image" {
  for_each = var.alertmanager_vms

  most_recent = true
  name        = each.value.image
}

resource "openstack_compute_servergroup_v2" "alertmanager_server_group" {
  name     = "alertmanager"
  policies = ["anti-affinity"]
}

data "openstack_networking_network_v2" "lan_flat_cloudinstances2b" {
  name = "lan-flat-cloudinstances2b"
}

data "openstack_networking_secgroup_v2" "default" {
  name = "default"
}

resource "openstack_networking_secgroup_v2" "alertmanager_security_group" {
  name        = "alertmanager"
  description = "Alert servers sending out notifications"
}

resource "openstack_networking_secgroup_rule_v2" "alertmanager_karma_access" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  security_group_id = openstack_networking_secgroup_v2.alertmanager_security_group.id
  remote_group_id   = var.haproxy_security_group_id
  description       = "karma dashboard"
}

resource "openstack_networking_secgroup_rule_v2" "alertmanager_trusted_project_proxy" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 8643
  port_range_max    = 8643
  security_group_id = openstack_networking_secgroup_v2.alertmanager_security_group.id
  remote_ip_prefix  = "0.0.0.0/0"
  description       = "proxy for trusted projects sending out notifications (T304716)"
}

resource "openstack_networking_secgroup_rule_v2" "alertmanager_access_prometheus" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 9093
  port_range_max    = 9093
  security_group_id = openstack_networking_secgroup_v2.alertmanager_security_group.id
  remote_group_id   = var.prometheus_security_group_id
  description       = "alertmanager access for prometheus"
}

resource "openstack_networking_secgroup_rule_v2" "alertmanager_access_thanos_query" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 9093
  port_range_max    = 9093
  security_group_id = openstack_networking_secgroup_v2.alertmanager_security_group.id
  remote_group_id   = var.thanos_fe_security_group_id
  description       = "alertmanager access for thanos-query"
}

resource "openstack_networking_secgroup_rule_v2" "alertmanager_internal_clustering" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 9093
  port_range_max    = 9094
  security_group_id = openstack_networking_secgroup_v2.alertmanager_security_group.id
  remote_group_id   = openstack_networking_secgroup_v2.alertmanager_security_group.id
  description       = "alertmanager clustering"
}

resource "openstack_networking_secgroup_rule_v2" "alertmanager_internal_ircrelay" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 19190
  port_range_max    = 19190
  security_group_id = openstack_networking_secgroup_v2.alertmanager_security_group.id
  remote_group_id   = openstack_networking_secgroup_v2.alertmanager_security_group.id
  description       = "alertmanager irc relay"
}

resource "openstack_networking_secgroup_rule_v2" "alertmanager_prometheus_karma" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 19194
  port_range_max    = 19194
  security_group_id = openstack_networking_secgroup_v2.alertmanager_security_group.id
  remote_group_id   = var.prometheus_security_group_id
  description       = "alertmanager kthxbye (prometheus scraping)"
}

resource "openstack_networking_secgroup_rule_v2" "alertmanager_prometheus_kthxbye" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 19195
  port_range_max    = 19195
  security_group_id = openstack_networking_secgroup_v2.alertmanager_security_group.id
  remote_group_id   = var.prometheus_security_group_id
  description       = "alertmanager kthxbye (prometheus scraping)"
}

resource "openstack_compute_instance_v2" "alertmanager_vm" {
  for_each = var.alertmanager_vms

  name      = "metricsinfra-alertmanager-${each.key}"
  image_id  = data.openstack_images_image_v2.alertmanager_vm_image[each.key].id
  flavor_id = data.openstack_compute_flavor_v2.alertmanager_vm_flavor[each.key].id

  security_groups = [
    data.openstack_networking_secgroup_v2.default.name,
    openstack_networking_secgroup_v2.alertmanager_security_group.name,
  ]

  network {
    uuid = data.openstack_networking_network_v2.lan_flat_cloudinstances2b.id
  }

  scheduler_hints {
    group = openstack_compute_servergroup_v2.alertmanager_server_group.id
  }

  lifecycle {
    ignore_changes = [
      image_id,
      # TODO: remove once all hosts have been replaced
      scheduler_hints,
    ]
  }
}
