# Copyright 2022 Taavi Väänänen <hi@taavi.wtf>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

terraform {
  required_version = ">= 1.4.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.54.1"
    }
  }
}

variable "db_instance_name" {
  type = string
}

variable "db_instance_flavor" {
  type = string
}

variable "db_instance_size" {
  type = number
}

variable "db_instance_datastore_type" {
  type = string
}

variable "db_instance_datastore_version" {
  type = string
}

variable "controller_vm_ip" {
  type = string
}

variable "grafana_vm_ip" {
  type = string
}

variable "db_password_controller_user" {
  type      = string
  sensitive = true
}

variable "db_password_controller_admin" {
  type      = string
  sensitive = true
}

variable "db_password_grafana" {
  type      = string
  sensitive = true
}

data "openstack_compute_flavor_v2" "db_flavor" {
  name = var.db_instance_flavor
}

resource "openstack_db_instance_v1" "db" {
  name      = var.db_instance_name
  flavor_id = data.openstack_compute_flavor_v2.db_flavor.id
  size      = var.db_instance_size

  datastore {
    type    = var.db_instance_datastore_type
    version = var.db_instance_datastore_version
  }
}

resource "openstack_db_database_v1" "prometheusconfig" {
  name        = "prometheusconfig"
  instance_id = openstack_db_instance_v1.db.id
}

resource "openstack_db_user_v1" "controller_user" {
  name        = "configuser"
  instance_id = openstack_db_instance_v1.db.id
  host        = var.controller_vm_ip
  password    = var.db_password_controller_user
  databases   = ["prometheusconfig"]
}

resource "openstack_db_user_v1" "controller_admin" {
  name        = "configadmin"
  instance_id = openstack_db_instance_v1.db.id
  host        = var.controller_vm_ip
  password    = var.db_password_controller_admin
  databases   = ["prometheusconfig"]
}

resource "openstack_db_database_v1" "grafana" {
  name        = "grafana"
  instance_id = openstack_db_instance_v1.db.id
}

resource "openstack_db_user_v1" "grafana" {
  name        = "grafana"
  instance_id = openstack_db_instance_v1.db.id
  host        = var.grafana_vm_ip
  password    = var.db_password_grafana
  databases   = ["grafana"]
}
