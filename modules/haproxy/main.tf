# Copyright 2022 Taavi Väänänen <hi@taavi.wtf>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

terraform {
  required_version = ">= 1.4.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.54.1"
    }

    cloudvps = {
      source  = "terraform.wmcloud.org/registry/cloudvps"
      version = "~> 0.2.0"
    }
  }
}

variable "haproxy_vms" {
  type = map(object({
    image  = string,
    flavor = string,
    pooled = bool,
  }))
}

variable "prometheus_security_group_id" {
  type = string
}

variable "public_names" {
  type = list(string)
}

variable "public_domain" {
  type = string
}

variable "internal_dns_zone" {
  type = string
}

variable "internal_names" {
  type = list(string)
}

data "openstack_compute_flavor_v2" "haproxy_vm_flavor" {
  for_each = var.haproxy_vms

  name = each.value.flavor
}

data "openstack_images_image_v2" "haproxy_vm_image" {
  for_each = var.haproxy_vms

  most_recent = true
  name        = each.value.image
}

data "openstack_networking_network_v2" "lan_flat_cloudinstances2b" {
  name = "lan-flat-cloudinstances2b"
}

data "openstack_networking_secgroup_v2" "default" {
  name = "default"
}

resource "openstack_networking_secgroup_v2" "haproxy_security_group" {
  name        = "haproxy"
  description = "HAProxy load balancers distributing inbound and some internal traffic"
}

resource "openstack_networking_secgroup_rule_v2" "haproxy_prometheus_exporter" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 9901
  port_range_max    = 9901
  security_group_id = openstack_networking_secgroup_v2.haproxy_security_group.id
  remote_group_id   = var.prometheus_security_group_id
  description       = "haproxy (prometheus scraping)"
}

resource "openstack_networking_secgroup_rule_v2" "haproxy_access" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  security_group_id = openstack_networking_secgroup_v2.haproxy_security_group.id
  remote_ip_prefix  = "0.0.0.0/0"
  description       = "haproxy access"
}

resource "openstack_compute_instance_v2" "haproxy_vm" {
  for_each = var.haproxy_vms

  name      = "metricsinfra-haproxy-${each.key}"
  image_id  = data.openstack_images_image_v2.haproxy_vm_image[each.key].id
  flavor_id = data.openstack_compute_flavor_v2.haproxy_vm_flavor[each.key].id

  security_groups = [
    data.openstack_networking_secgroup_v2.default.name,
    openstack_networking_secgroup_v2.haproxy_security_group.name,
  ]

  network {
    uuid = data.openstack_networking_network_v2.lan_flat_cloudinstances2b.id
  }

  lifecycle {
    ignore_changes = [
      image_id,
    ]
  }
}

resource "cloudvps_web_proxy" "metricsinfra_public_access" {
  count = length(var.public_names)

  hostname = var.public_names[count.index]
  domain   = var.public_domain
  backends = [for i, data in var.haproxy_vms : "http://${openstack_compute_instance_v2.haproxy_vm[i].access_ip_v4}:80" if data.pooled]
}

data "openstack_dns_zone_v2" "svc" {
  name = var.internal_dns_zone
}

resource "openstack_dns_recordset_v2" "svc_config_manager_a" {
  count = length(var.internal_names)

  zone_id     = data.openstack_dns_zone_v2.svc.id
  name        = "${var.internal_names[count.index]}.${var.internal_dns_zone}"
  description = ""
  ttl         = 300
  type        = "A"
  records     = [for i, data in var.haproxy_vms : openstack_compute_instance_v2.haproxy_vm[i].access_ip_v4 if data.pooled]
}

resource "cloudvps_puppet_prefix" "haproxy" {
  name  = "metricsinfra-haproxy-"
  roles = ["role::wmcs::metricsinfra::haproxy"]
}
