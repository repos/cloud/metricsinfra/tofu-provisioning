# Copyright 2023-2024 Taavi Väänänen <hi@taavi.wtf>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

terraform {
  required_version = ">= 1.4.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.54.1"
    }

    cloudvps = {
      source  = "terraform.wmcloud.org/registry/cloudvps"
      version = "~> 0.2.0"
    }
  }
}

variable "puppetserver_vm_active" { type = string }
variable "puppetserver_vms" {
  type = map(object({
    image  = string,
    flavor = string,
  }))
}

variable "puppetserver_data_volume_size" { type = number }

data "openstack_compute_flavor_v2" "puppetserver_vm_flavor" {
  for_each = var.puppetserver_vms

  name = each.value.flavor
}

data "openstack_images_image_v2" "puppetserver_vm_image" {
  for_each = var.puppetserver_vms

  most_recent = true
  name        = each.value.image
}

data "openstack_networking_network_v2" "lan_flat_cloudinstances2b" {
  name = "lan-flat-cloudinstances2b"
}

data "openstack_networking_secgroup_v2" "default" {
  name = "default"
}

resource "openstack_compute_servergroup_v2" "puppetserver_server_group" {
  name     = "puppetserver"
  policies = ["anti-affinity"]
}

resource "openstack_networking_secgroup_v2" "puppetserver_security_group" {
  name        = "puppetserver"
  description = "Configuration for the whole cluster"
}

resource "openstack_networking_secgroup_rule_v2" "puppetserver_access" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 8140
  port_range_max    = 8140
  security_group_id = openstack_networking_secgroup_v2.puppetserver_security_group.id
  remote_group_id   = data.openstack_networking_secgroup_v2.default.id
  description       = "puppet"
}

resource "openstack_blockstorage_volume_v3" "puppetserver_data" {
  name        = "metricsinfra-puppet"
  size        = var.puppetserver_data_volume_size
  volume_type = "standard"
  description = "certs and git repo for metricsinfra puppet server"
}

resource "openstack_compute_instance_v2" "puppetserver_vm" {
  for_each = var.puppetserver_vms

  name      = "metricsinfra-puppetserver-${each.key}"
  image_id  = data.openstack_images_image_v2.puppetserver_vm_image[each.key].id
  flavor_id = data.openstack_compute_flavor_v2.puppetserver_vm_flavor[each.key].id

  security_groups = [
    data.openstack_networking_secgroup_v2.default.name,
    openstack_networking_secgroup_v2.puppetserver_security_group.name,
  ]

  network {
    uuid = data.openstack_networking_network_v2.lan_flat_cloudinstances2b.id
  }

  scheduler_hints {
    group = openstack_compute_servergroup_v2.puppetserver_server_group.id
  }

  lifecycle {
    ignore_changes = [
      image_id,
      key_pair,
      # TODO: remove once all hosts have been replaced
      scheduler_hints,
    ]
  }
}

# TODO: figure out how to import this
#resource "openstack_compute_volume_attach_v2" "puppetserver_data" {
#  instance_id = openstack_compute_instance_v2.puppetserver_vm[var.puppetserver_vm_active].id
#  volume_id   = openstack_blockstorage_volume_v3.puppetserver_data.id
#}

resource "cloudvps_puppet_prefix" "puppetserver" {
  name  = "metricsinfra-puppetserver-"
  roles = ["role::puppetserver::cloud_vps_project"]
  hiera = file("${path.module}/puppetserver_hiera.yaml")
}
