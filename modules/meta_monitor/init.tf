# Copyright 2023 Taavi Väänänen for the Wikimedia Foundation, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

terraform {
  required_version = ">= 1.4.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.54.1"
    }

    cloudvps = {
      source  = "terraform.wmcloud.org/registry/cloudvps"
      version = "~> 0.2.0"
    }
  }
}

variable "meta_monitor_vms" {
  type = map(object({
    image  = string,
    flavor = string,
  }))
}

variable "haproxy_security_group_id" {
  type = string
}

data "openstack_compute_flavor_v2" "meta_monitor_vm_flavor" {
  for_each = var.meta_monitor_vms

  name = each.value.flavor
}

data "openstack_images_image_v2" "meta_monitor_vm_image" {
  for_each = var.meta_monitor_vms

  most_recent = true
  name        = each.value.image
}

data "openstack_networking_network_v2" "lan_flat_cloudinstances2b" {
  name = "lan-flat-cloudinstances2b"
}

data "openstack_networking_secgroup_v2" "default" {
  name = "default"
}

resource "openstack_compute_servergroup_v2" "meta_monitor_server_group" {
  name     = "meta-monitor"
  policies = ["anti-affinity"]
}

resource "openstack_networking_secgroup_v2" "meta_monitor_security_group" {
  name        = "meta-monitor"
  description = "Meta monitoring endpoint"
}

resource "openstack_networking_secgroup_rule_v2" "meta_monitor_haproxy_access" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  security_group_id = openstack_networking_secgroup_v2.meta_monitor_security_group.id
  remote_group_id   = var.haproxy_security_group_id
  description       = "meta-monitoring haproxy access"
}

resource "openstack_compute_instance_v2" "meta_monitor_vm" {
  for_each = var.meta_monitor_vms

  name      = "metricsinfra-meta-monitor-${each.key}"
  image_id  = data.openstack_images_image_v2.meta_monitor_vm_image[each.key].id
  flavor_id = data.openstack_compute_flavor_v2.meta_monitor_vm_flavor[each.key].id

  security_groups = [
    data.openstack_networking_secgroup_v2.default.name,
    openstack_networking_secgroup_v2.meta_monitor_security_group.name,
  ]

  network {
    uuid = data.openstack_networking_network_v2.lan_flat_cloudinstances2b.id
  }

  scheduler_hints {
    group = openstack_compute_servergroup_v2.meta_monitor_server_group.id
  }

  lifecycle {
    ignore_changes = [
      image_id,
    ]
  }
}

resource "cloudvps_puppet_prefix" "meta_monitor" {
  name  = "metricsinfra-meta-monitor-"
  roles = ["role::wmcs::metricsinfra::meta_monitor"]
}
