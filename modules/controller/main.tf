# Copyright 2022 Taavi Väänänen <hi@taavi.wtf>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

terraform {
  required_version = ">= 1.4.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.54.1"
    }
  }
}

variable "controller_vm_generation" {
  type = number
}

variable "controller_vm_image_name" {
  type = string
}

variable "controller_vm_flavor_name" {
  type = string
}

variable "haproxy_security_group_id" {
  type = string
}

variable "prometheus_security_group_id" {
  type = string
}

data "openstack_compute_flavor_v2" "controller_vm_flavor" {
  name = var.controller_vm_flavor_name
}

data "openstack_images_image_v2" "controller_vm_image" {
  most_recent = true
  name        = var.controller_vm_image_name
}

data "openstack_networking_network_v2" "lan_flat_cloudinstances2b" {
  name = "lan-flat-cloudinstances2b"
}

data "openstack_networking_secgroup_v2" "default" {
  name = "default"
}

resource "openstack_networking_secgroup_v2" "controller_security_group" {
  name        = "controller"
  description = "Configuration management application lives here"
}

resource "openstack_networking_secgroup_rule_v2" "controller_prometheus" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  security_group_id = openstack_networking_secgroup_v2.controller_security_group.id
  remote_group_id   = var.prometheus_security_group_id
  description       = "prometheus-manager (prometheus scraping)"
}

resource "openstack_networking_secgroup_rule_v2" "controller_access" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  security_group_id = openstack_networking_secgroup_v2.controller_security_group.id
  remote_group_id   = var.haproxy_security_group_id
  description       = "prometheus-manager access"
}

resource "openstack_compute_instance_v2" "controller_vm" {
  name      = "metricsinfra-controller-${var.controller_vm_generation}"
  image_id  = data.openstack_images_image_v2.controller_vm_image.id
  flavor_id = data.openstack_compute_flavor_v2.controller_vm_flavor.id

  security_groups = [
    data.openstack_networking_secgroup_v2.default.name,
    openstack_networking_secgroup_v2.controller_security_group.name,
  ]

  network {
    uuid = data.openstack_networking_network_v2.lan_flat_cloudinstances2b.id
  }

  lifecycle {
    ignore_changes = [
      image_id,
    ]
  }
}
