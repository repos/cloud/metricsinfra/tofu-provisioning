# Copyright 2022 Taavi Väänänen <hi@taavi.wtf>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

terraform {
  required_version = ">= 1.4.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.54.1"
    }
  }
}

variable "thanos_fe_vms" {
  type = map(object({
    image  = string,
    flavor = string,
  }))
}

variable "prometheus_security_group_id" {
  type = string
}

variable "haproxy_security_group_id" {
  type = string
}

data "openstack_compute_flavor_v2" "thanos_fe_vm_flavor" {
  for_each = var.thanos_fe_vms

  name = each.value.flavor
}

data "openstack_images_image_v2" "thanos_fe_vm_image" {
  for_each = var.thanos_fe_vms

  most_recent = true
  name        = each.value.image
}

data "openstack_networking_network_v2" "lan_flat_cloudinstances2b" {
  name = "lan-flat-cloudinstances2b"
}

data "openstack_networking_secgroup_v2" "default" {
  name = "default"
}

resource "openstack_compute_servergroup_v2" "thanos_fe_server_group" {
  name     = "thanos-fe"
  policies = ["anti-affinity"]
}

resource "openstack_networking_secgroup_v2" "thanos_fe_security_group" {
  name        = "thanos-fe"
  description = "Thanos for load balancing Prometheus queries"
}

resource "openstack_networking_secgroup_rule_v2" "thanos_fe_query_haproxy_access" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 10902
  port_range_max    = 10902
  security_group_id = openstack_networking_secgroup_v2.thanos_fe_security_group.id
  remote_group_id   = var.haproxy_security_group_id
  description       = "thanos-query"
}

resource "openstack_networking_secgroup_rule_v2" "thanos_fe_query_prometheus" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 10902
  port_range_max    = 10902
  security_group_id = openstack_networking_secgroup_v2.thanos_fe_security_group.id
  remote_group_id   = var.prometheus_security_group_id
  description       = "thanos-query"
}

resource "openstack_networking_secgroup_rule_v2" "thanos_fe_rule_haproxy_access" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 17901
  port_range_max    = 17901
  security_group_id = openstack_networking_secgroup_v2.thanos_fe_security_group.id
  remote_group_id   = var.haproxy_security_group_id
  description       = "thanos-rule"
}

resource "openstack_networking_secgroup_rule_v2" "thanos_fe_rule_prometheus" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 17902
  port_range_max    = 17902
  security_group_id = openstack_networking_secgroup_v2.thanos_fe_security_group.id
  remote_group_id   = var.prometheus_security_group_id
  description       = "thanos-rule"
}

resource "openstack_compute_instance_v2" "thanos_fe_vm" {
  for_each = var.thanos_fe_vms

  name      = "metricsinfra-thanos-fe-${each.key}"
  image_id  = data.openstack_images_image_v2.thanos_fe_vm_image[each.key].id
  flavor_id = data.openstack_compute_flavor_v2.thanos_fe_vm_flavor[each.key].id

  security_groups = [
    data.openstack_networking_secgroup_v2.default.name,
    openstack_networking_secgroup_v2.thanos_fe_security_group.name,
  ]

  network {
    uuid = data.openstack_networking_network_v2.lan_flat_cloudinstances2b.id
  }

  scheduler_hints {
    group = openstack_compute_servergroup_v2.thanos_fe_server_group.id
  }

  lifecycle {
    ignore_changes = [
      image_id,
      # TODO: remove once all hosts have been replaced
      scheduler_hints,
    ]
  }
}
