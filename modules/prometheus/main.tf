# Copyright 2022 Taavi Väänänen <hi@taavi.wtf>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

terraform {
  required_version = ">= 1.4.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.54.1"
    }
  }
}

variable "cinder_volumes" {
  type = map(number)
}

variable "prometheus_vms" {
  type = map(object({
    image         = string,
    flavor        = string,
    cinder_volume = string,
    port_id       = string,
  }))
}

variable "thanos_fe_security_group_id" {
  type = string
}

data "openstack_networking_secgroup_v2" "default" {
  name = "default"
}

resource "openstack_networking_secgroup_v2" "prometheus" {
  name        = "prometheus"
  description = "Prometheus servers doing the actual scraping"
}

data "openstack_images_image_v2" "prometheus_vm_image" {
  for_each = var.prometheus_vms

  most_recent = true
  name        = each.value.image
}

data "openstack_compute_flavor_v2" "prometheus_vm_flavor" {
  for_each = var.prometheus_vms

  name = each.value.flavor
}

resource "openstack_blockstorage_volume_v3" "prometheus_data" {
  for_each = var.cinder_volumes

  name        = "prometheus-${each.key}"
  size        = each.value
  volume_type = "standard"
}

resource "openstack_compute_instance_v2" "prometheus_vm" {
  for_each = var.prometheus_vms

  name      = "metricsinfra-prometheus-${each.key}"
  image_id  = data.openstack_images_image_v2.prometheus_vm_image[each.key].id
  flavor_id = data.openstack_compute_flavor_v2.prometheus_vm_flavor[each.key].id

  security_groups = [
    data.openstack_networking_secgroup_v2.default.name,
    openstack_networking_secgroup_v2.prometheus.name,
  ]

  network {
    port = each.value.port_id
  }

  lifecycle {
    ignore_changes = [
      image_id,
      network[0].port,
    ]
  }
}

resource "openstack_compute_volume_attach_v2" "prometheus_vm_data" {
  for_each = var.prometheus_vms

  instance_id = openstack_compute_instance_v2.prometheus_vm[each.key].id
  volume_id   = openstack_blockstorage_volume_v3.prometheus_data[each.value.cinder_volume].id
}

resource "openstack_networking_secgroup_rule_v2" "prometheus_thanos_sidecar_http" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 19900
  port_range_max    = 19900
  security_group_id = openstack_networking_secgroup_v2.prometheus.id
  remote_group_id   = openstack_networking_secgroup_v2.prometheus.id
  description       = "thanos-sidecar http (prometheus scraping)"
}

resource "openstack_networking_secgroup_rule_v2" "prometheus_thanos_sidecar_grpc" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 29900
  port_range_max    = 29900
  security_group_id = openstack_networking_secgroup_v2.prometheus.id
  remote_group_id   = var.thanos_fe_security_group_id
  description       = "thanos-sidecar grpc"
}

resource "openstack_networking_secgroup_rule_v2" "prometheus_self_http" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  security_group_id = openstack_networking_secgroup_v2.prometheus.id
  remote_group_id   = openstack_networking_secgroup_v2.prometheus.id
  description       = "prometheus http (prometheus scraping)"
}

resource "openstack_networking_secgroup_rule_v2" "prometheus_blackbox_http" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 9115
  port_range_max    = 9115
  security_group_id = openstack_networking_secgroup_v2.prometheus.id
  remote_group_id   = openstack_networking_secgroup_v2.prometheus.id
  description       = "prometheus blackbox exporter (prometheus scraping)"
}
