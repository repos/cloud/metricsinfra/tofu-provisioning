# Copyright 2022-2024 Taavi Väänänen <hi@taavi.wtf>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

terraform {
  required_version = ">= 1.4.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.54.1"
    }

    cloudvps = {
      source  = "terraform.wmcloud.org/registry/cloudvps"
      version = "~> 0.2.0"
    }
  }

  backend "s3" {
    region   = "eqiad1"
    bucket   = "metricsinfra:tf-state"
    endpoint = "https://object.eqiad1.wikimediacloud.org"
    key      = "state/main"

    skip_region_validation      = true
    skip_credentials_validation = true
    force_path_style            = true
  }
}

variable "os_auth_url" { type = string }
variable "os_project_id" { type = string }
variable "os_application_credential_id" { type = string }
variable "os_application_credential_secret" {
  type      = string
  sensitive = true
}

variable "db_password_controller_user" {
  type      = string
  sensitive = true
}

variable "db_password_controller_admin" {
  type      = string
  sensitive = true
}

variable "db_password_grafana" {
  type      = string
  sensitive = true
}

provider "openstack" {
  auth_url                      = var.os_auth_url
  tenant_id                     = var.os_project_id
  application_credential_id     = var.os_application_credential_id
  application_credential_secret = var.os_application_credential_secret
}

provider "cloudvps" {
  os_auth_url                      = var.os_auth_url
  os_project_id                    = var.os_project_id
  os_application_credential_id     = var.os_application_credential_id
  os_application_credential_secret = var.os_application_credential_secret
}

module "puppetserver" {
  source = "./modules/puppetserver"

  puppetserver_vm_active = "1"
  puppetserver_vms = {
    "1" = {
      image  = "debian-12.0-bookworm"
      flavor = "g4.cores2.ram4.disk20"
    }
  }

  puppetserver_data_volume_size = 7
}

module "database" {
  source = "./modules/database"

  db_instance_name   = "metricsinfra-db-1"
  db_instance_flavor = "g3.cores1.ram2.disk20"
  db_instance_size   = 2

  db_instance_datastore_type    = "mariadb"
  db_instance_datastore_version = "10.5.10"

  db_password_controller_user  = var.db_password_controller_user
  db_password_controller_admin = var.db_password_controller_admin
  db_password_grafana          = var.db_password_grafana

  controller_vm_ip = module.controller.controller_vm_ip
  grafana_vm_ip    = module.grafana.grafana_vm_ip
}

module "controller" {
  source = "./modules/controller"

  controller_vm_generation  = 2
  controller_vm_image_name  = "debian-11.0-bullseye"
  controller_vm_flavor_name = "g4.cores1.ram2.disk20"

  prometheus_security_group_id = module.prometheus.prometheus_security_group_id

  haproxy_security_group_id = module.haproxy.haproxy_security_group_id
}

module "haproxy" {
  source = "./modules/haproxy"

  haproxy_vms = {
    "2" = {
      image  = "debian-12.0-bookworm"
      flavor = "g4.cores1.ram2.disk20"
      pooled = true
    }
  }

  prometheus_security_group_id = module.prometheus.prometheus_security_group_id

  public_names  = ["prometheus", "prometheus-alerts", "grafana", "grafana-rw", "metricsinfra-metamonitor", "metricsinfra-alertmanager-rw"]
  public_domain = "wmcloud.org"

  internal_names    = ["config-manager", "prometheus"]
  internal_dns_zone = "svc.metricsinfra.eqiad1.wikimedia.cloud."
}

module "prometheus" {
  source = "./modules/prometheus"

  cinder_volumes = {
    "a" = 150
    "b" = 150
  }

  prometheus_vms = {
    "2" = {
      image         = "debian-11.0-bullseye",
      flavor        = "g4.cores8.ram16.disk20",
      port_id       = "02099d16-afab-4f27-83fc-8ffe0663e421",
      cinder_volume = "a",
    },
    "3" = {
      image         = "debian-11.0-bullseye",
      flavor        = "g4.cores8.ram16.disk20",
      port_id       = "2e67a486-e840-4800-b974-d9220f5e107a",
      cinder_volume = "b"
    },
  }

  thanos_fe_security_group_id = module.thanos_fe.thanos_fe_security_group_id
}

module "alertmanager" {
  source = "./modules/alertmanager"

  alertmanager_vms = {
    "2" = {
      image  = "debian-12.0-bookworm"
      flavor = "g4.cores1.ram2.disk20"
    }
    "3" = {
      image  = "debian-12.0-bookworm"
      flavor = "g4.cores1.ram2.disk20"
    }
  }

  prometheus_security_group_id = module.prometheus.prometheus_security_group_id
  haproxy_security_group_id    = module.haproxy.haproxy_security_group_id
  thanos_fe_security_group_id  = module.thanos_fe.thanos_fe_security_group_id
}

module "thanos_fe" {
  source = "./modules/thanos_fe"

  thanos_fe_vms = {
    "1" = {
      image  = "debian-11.0-bullseye"
      flavor = "g4.cores2.ram4.disk20"
    }
  }

  prometheus_security_group_id = module.prometheus.prometheus_security_group_id
  haproxy_security_group_id    = module.haproxy.haproxy_security_group_id
}

module "grafana" {
  source = "./modules/grafana"

  grafana_vm_generation  = 1
  grafana_vm_image_name  = "debian-11.0-bullseye"
  grafana_vm_flavor_name = "g4.cores1.ram2.disk20"

  prometheus_security_group_id = module.prometheus.prometheus_security_group_id
  haproxy_security_group_id    = module.haproxy.haproxy_security_group_id
}

module "meta_monitor" {
  source = "./modules/meta_monitor"

  meta_monitor_vms = {
    "1" = {
      image  = "debian-12.0-bookworm"
      flavor = "g4.cores1.ram2.disk20"
    }
  }

  haproxy_security_group_id = module.haproxy.haproxy_security_group_id
}
